package br.com.treinamento.dojo.controller;

import java.net.URI;
import java.net.URLEncoder;
import java.util.List;

import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.treinamento.dojo.model.*;
import br.com.treinamento.dojo.model.Character;

@RestController
public class WelcomeController {
	
	public Response requestURL(URI url) {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(url);
		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON_TYPE);
		Response response = invocationBuilder.get();
		return response;

	}

	public String processResultCharacteres(String name) {
		Response response = requestURL(getURLCharacteres(name));
		CharacterDataWrapper characterDataWrapper = response.readEntity(CharacterDataWrapper.class);
		CharacterDataContainer dataCharacter = characterDataWrapper.getData();
		List<Character> Charateres = dataCharacter.getResults();
		Character Character = Charateres.get(0);
		return Integer.toString(Character.getId());
	}

	public String processResultStories(String idCharacter) {
		Response resultStories = requestURL(getURLStories(idCharacter));
		StoryDataWrapper storyDataWrapper = resultStories.readEntity(StoryDataWrapper.class);
		StoryDataContainer storyDataContainer = storyDataWrapper.getData();
		List<Story> stories = storyDataContainer.getResults();
		Story story = stories.get(0);
		ComicSummary originalIssue = story.getOriginalIssue();
		return originalIssue.getName();
	}

	public URI getURLCharacteres(String name) {
		URI url = UriBuilder
				.fromUri("http://gateway.marvel.com:80/v1/public/characters?name=" + name
						+ "&ts=1&apikey=571d045a0dfafd1cc7de9bb4fd5d69b0&hash=f71ec470bf6573eb10a8586b40c7b9bd")
				.build();
		return url;
	}

	public URI getURLStories(String idCharacter) {
		URI url = UriBuilder
				.fromUri("http://gateway.marvel.com:80/v1/public/stories?characters=" + idCharacter
						+ "&ts=1&apikey=571d045a0dfafd1cc7de9bb4fd5d69b0&hash=f71ec470bf6573eb10a8586b40c7b9bd")
				.build();
		return url;
	}

	@RequestMapping(value = "/hero", method = RequestMethod.GET)
	public ResponseEntity<String> helloWorld(@QueryParam("name") String name) {
		try {
			String nameEncoded = URLEncoder.encode(name, java.nio.charset.StandardCharsets.UTF_8.toString());
			String idCharacter = processResultCharacteres(nameEncoded);
			String originalIssue = processResultStories(idCharacter);
			return new ResponseEntity<String>(originalIssue, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(e.toString(), HttpStatus.OK);
		}

	}

}
