package br.com.treinamento.dojo.model;

import java.util.List;

public class CreatorList {

	private int available;
	private int returned;
	private String collectionURI;
	private List<CreatorSummary> items;
	public int getAvailable() {
		return available;
	}
	public void setAvailable(int available) {
		this.available = available;
	}
	public int getReturned() {
		return returned;
	}
	public void setReturned(int returned) {
		this.returned = returned;
	}
	public String getCollectionURI() {
		return collectionURI;
	}
	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}
	public List<CreatorSummary> getItems() {
		return items;
	}
	public void setItems(List<CreatorSummary> items) {
		this.items = items;
	}
	
}
